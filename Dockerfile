FROM debian:11 AS builder

ENV maj_release=1.16
ENV release=1.16.5

RUN apt update -y \
    && apt install -y curl bzip2 libboost-dev libboost-program-options-dev libboost-context-dev libboost-locale-dev libboost-coroutine-dev libboost-filesystem-dev libboost-random-dev libboost-system-dev g++ libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libbz2-dev zlib1g-dev libcrypto++-dev libboost-iostreams1.74-dev libcairo2-dev libpango1.0-dev libssl-dev scons \
    && curl -L https://sourceforge.net/projects/wesnoth/files/wesnoth-${maj_release}/wesnoth-${release}/wesnoth-${release}.tar.bz2/download --output wesnoth-${release}.tar.bz2 \
    && tar jxf wesnoth-${release}.tar.bz2 \
    && cd wesnoth-${release} \
    && scons --config=force wesnothd \
    && cp wesnothd ..

FROM debian:11

WORKDIR /root/
COPY --from=builder ./wesnothd ./
COPY ./config/wesnothd.conf ./
RUN chmod +x wesnothd \
    && apt update -y \
    && apt install -y tini libcairo2 libboost-filesystem1.74.0 libboost-locale1.74.0 libboost-coroutine1.74.0 libboost-program-options1.74.0 libboost-context1.74.0 libboost-random1.74.0 libboost-iostreams1.74.0

ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["./wesnothd","-c","wesnothd.conf"]
